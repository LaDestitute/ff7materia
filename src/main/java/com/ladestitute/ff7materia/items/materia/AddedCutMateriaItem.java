package com.ladestitute.ff7materia.items.materia;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class AddedCutMateriaItem extends Item {

    public AddedCutMateriaItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Attacks simultaneously when paired Materia is used"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
