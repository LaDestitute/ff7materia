package com.ladestitute.ff7materia.util;

import com.ladestitute.ff7materia.FF7MateriaMain;
import net.minecraft.client.KeyMapping;
import net.neoforged.neoforge.client.event.RegisterKeyMappingsEvent;
import org.lwjgl.glfw.GLFW;

public class FF7MateriaKeyboardUtil {

    public static KeyMapping LOCK_ON;
    public static KeyMapping CANCEL_LOCK_ON;
    public static KeyMapping ONE_KEY;
    public static KeyMapping TWO_KEY;
    public static KeyMapping KP_ONE_KEY;
    public static KeyMapping KP_TWO_KEY;

    public static void keyBind(RegisterKeyMappingsEvent e) {
        LOCK_ON = new KeyMapping("key." + FF7MateriaMain.MODID + ".lock_on", GLFW.GLFW_KEY_Z, "key.categories." + FF7MateriaMain.MODID);
        CANCEL_LOCK_ON = new KeyMapping("key." + FF7MateriaMain.MODID + ".cancel_lock_on", GLFW.GLFW_KEY_X, "key.categories." + FF7MateriaMain.MODID);
        ONE_KEY = new KeyMapping("key." + FF7MateriaMain.MODID + ".one_key", GLFW.GLFW_KEY_1,
                "key.categories." + FF7MateriaMain.MODID);
        TWO_KEY = new KeyMapping("key." + FF7MateriaMain.MODID + ".two_key", GLFW.GLFW_KEY_2,
                "key.categories." + FF7MateriaMain.MODID);
        KP_ONE_KEY = new KeyMapping("key." + FF7MateriaMain.MODID + ".kp_one_key", GLFW.GLFW_KEY_KP_1,
                "key.categories." + FF7MateriaMain.MODID);
        KP_TWO_KEY = new KeyMapping("key." + FF7MateriaMain.MODID + ".kp_two_key", GLFW.GLFW_KEY_KP_2,
                "key.categories." + FF7MateriaMain.MODID);
        e.register(LOCK_ON);
        e.register(CANCEL_LOCK_ON);
        e.register(ONE_KEY);
        e.register(TWO_KEY);
        e.register(KP_ONE_KEY);
        e.register(KP_TWO_KEY);
    }

}
