package com.ladestitute.ff7materia.event;

import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import com.ladestitute.ff7materia.util.FF7MateriaKeyboardUtil;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.TickEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.living.LivingEvent;

import java.util.Optional;

public class TargetingHandler {
    @SubscribeEvent
    public void targeting(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);

        //WIP targeting system
        if (FF7MateriaKeyboardUtil.LOCK_ON.consumeClick() && event.player.getData(AttachmentInit.TARGET_LIMIT) == 0) {
            //Target code
            {
                float playerRotX = event.player.getXRot();
                float playerRotY = event.player.getYRot();
                Vec3 startPos = event.player.getEyePosition();
                float f2 = Mth.cos(-playerRotY * ((float) Math.PI / 180F) - (float) Math.PI);
                float f3 = Mth.sin(-playerRotY * ((float) Math.PI / 180F) - (float) Math.PI);
                float f4 = -Mth.cos(-playerRotX * ((float) Math.PI / 180F));
                float additionY = Mth.sin(-playerRotX * ((float) Math.PI / 180F));
                float additionX = f3 * f4;
                float additionZ = f2 * f4;
                double d0 = 16;
                Vec3 endVec = startPos.add((double) additionX * d0, (double) additionY * d0, (double) additionZ * d0);
                AABB startEndBox = new AABB(startPos, endVec);
                LivingEntity entity = null;
                for (Entity entity1 : event.player.level().getEntities(event.player, startEndBox, (val) -> true)) {
                    AABB aabb = entity1.getBoundingBox().inflate(entity1.getPickRadius());
                    Optional<Vec3> optional = aabb.clip(startPos, endVec);
                    if (aabb.contains(startPos)) {
                        if (d0 >= 0.0D) {
                            entity = (LivingEntity) entity1;
                            startPos = optional.orElse(startPos);
                            d0 = 0.0D;
                        }
                    } else if (optional.isPresent()) {
                        Vec3 vec31 = optional.get();
                        double d1 = startPos.distanceToSqr(vec31);
                        if (d1 < d0 || d0 == 0.0D) {
                            if (entity1.getRootVehicle() == event.player.getRootVehicle() && !entity1.canRiderInteract()) {
                                if (d0 == 0.0D) {
                                    entity = (LivingEntity) entity1;
                                    startPos = vec31;
                                }
                            } else {
                                entity = (LivingEntity) entity1;
                                startPos = vec31;
                                d0 = d1;
                            }
                        }
                    }
                }

                if (entity != null)
                {
                    materia_stats.is_targeting(event.player, true, true);
                    entity.setData(AttachmentInit.TARGETED, true);
                    materia_stats.has_target(event.player, 1, true);
                    if(entity.getData(AttachmentInit.TARGETED)) {
                        System.out.println("ENTITY IS TARGET");
                        entity.addEffect(new MobEffectInstance(MobEffects.GLOWING, 32147, 0));
                    }
                }
            }
            //Target code
        }
        if (FF7MateriaKeyboardUtil.CANCEL_LOCK_ON.consumeClick())
        {
            //Target code
            {
                float playerRotX = event.player.getXRot();
                float playerRotY = event.player.getYRot();
                Vec3 startPos = event.player.getEyePosition();
                float f2 = Mth.cos(-playerRotY * ((float) Math.PI / 180F) - (float) Math.PI);
                float f3 = Mth.sin(-playerRotY * ((float) Math.PI / 180F) - (float) Math.PI);
                float f4 = -Mth.cos(-playerRotX * ((float) Math.PI / 180F));
                float additionY = Mth.sin(-playerRotX * ((float) Math.PI / 180F));
                float additionX = f3 * f4;
                float additionZ = f2 * f4;
                double d0 = 16;
                Vec3 endVec = startPos.add((double) additionX * d0, (double) additionY * d0, (double) additionZ * d0);
                AABB startEndBox = new AABB(startPos, endVec);
                LivingEntity entity = null;
                for (Entity entity1 : event.player.level().getEntities(event.player, startEndBox, (val) -> true)) {
                    AABB aabb = entity1.getBoundingBox().inflate(entity1.getPickRadius());
                    Optional<Vec3> optional = aabb.clip(startPos, endVec);
                    if (aabb.contains(startPos)) {
                        if (d0 >= 0.0D) {
                            entity = (LivingEntity) entity1;
                            startPos = optional.orElse(startPos);
                            d0 = 0.0D;
                        }
                    } else if (optional.isPresent()) {
                        Vec3 vec31 = optional.get();
                        double d1 = startPos.distanceToSqr(vec31);
                        if (d1 < d0 || d0 == 0.0D) {
                            if (entity1.getRootVehicle() == event.player.getRootVehicle() && !entity1.canRiderInteract()) {
                                if (d0 == 0.0D) {
                                    entity = (LivingEntity) entity1;
                                    startPos = vec31;
                                }
                            } else {
                                entity = (LivingEntity) entity1;
                                startPos = vec31;
                                d0 = d1;
                            }
                        }
                    }
                }

                if (entity != null) {
                    materia_stats.is_targeting(event.player, false, true);
                    entity.setData(AttachmentInit.TARGETED, false);
                    materia_stats.has_target(event.player, 0, true);
                    if(!entity.getData(AttachmentInit.TARGETED)) {
                        System.out.println("ENTITY IS NOT TARGET");
                        entity.removeEffect(MobEffects.GLOWING);
                    }
                }
            }
            //Target code
        }


    }

    public void targetingchecks(LivingDeathEvent event) {
        //Remove target restrictions if the player kills the target
        //Todo: clear target restrictions on login
        if (event.getSource().getEntity() instanceof Player) {
            event.getSource().getEntity().setData(AttachmentInit.TARGETING, false);
            event.getSource().getEntity().setData(AttachmentInit.TARGET_LIMIT, 0);
        }
    }

    public void livingevent(LivingEvent.LivingTickEvent event)
    {
        if(event.getEntity().getData(AttachmentInit.TARGETED))
        {
            event.getEntity().addEffect(new MobEffectInstance(MobEffects.GLOWING, 32147, 0));
        }
       // else event.getEntity().removeEffect(MobEffects.GLOWING);
    }
}
