package com.ladestitute.ff7materia.event;

import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import com.ladestitute.ff7materia.registry.ItemInit;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.TickEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

public class CoreMateriaHandler {

    @SubscribeEvent
    public void onClone(PlayerEvent.Clone event) {
        Player oldPlayer = event.getOriginal();
        Player newPlayer = event.getEntity();
        if (!event.isWasDeath()) {
            PlayerMateriaStats materiastats = oldPlayer.getData(AttachmentInit.PLAYER_MATERIA_STATS);
            PlayerMateriaStats materiastats2 = newPlayer.getData(AttachmentInit.PLAYER_MATERIA_STATS);
            materiastats2.deserializeNBT(materiastats.serializeNBT());
        }
    }

    @SuppressWarnings("all")
    @SubscribeEvent
    public void spellslots(TickEvent.PlayerTickEvent event) {
        //WIP system to determine spell slot order based on equipped materia
        //This will also consider All/Quadra materia

        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        //Todo with +1.21: replace with non-deprecated method
        ItemStack fire_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.FIRE_MATERIA.get()).map(SlotResult::stack).orElse(null);
        ItemStack ice_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.ICE_MATERIA.get()).map(SlotResult::stack).orElse(null);
        ItemStack earth_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.EARTH_MATERIA.get()).map(SlotResult::stack).orElse(null);
        ItemStack lightning_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.LIGHTNING_MATERIA.get()).map(SlotResult::stack).orElse(null);
        ItemStack restore_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.RESTORE_MATERIA.get()).map(SlotResult::stack).orElse(null);
        ItemStack added_cut_materia =
                CuriosApi.getCuriosHelper().findFirstCurio(event.player,
                        ItemInit.ADDED_CUT_MATERIA.get()).map(SlotResult::stack).orElse(null);

        //Empty check
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Slot one is empty
                        if (slot0.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 0, true);
                          //  System.out.println("SLOT ONE IS EMPTY");
                        }
                        //Slot two is empty
                        if (slot1.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 0, true);
                          //  System.out.println("SLOT TWO IS EMPTY");
                        }

                    }
                });
        });
        //Empty check

//        //Check for fire materia
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            if (iCuriosItemHandler.findFirstCurio(ItemInit.FIRE_MATERIA.get()).isPresent()) {
                iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Check in slot one
                        if (slot0 == fire_materia
                                && !fire_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 1, true);
                          //  System.out.println("SLOT ONE IS FIRE");
                        }
                        //Check in slot two
                        if (slot1 == fire_materia
                                && !fire_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 1, true);
                          //  System.out.println("SLOT TWO IS FIRE");
                        }
                    }
                });
            }
        });
//        //Check for fire materia
//
//        //Check for ice materia
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            if (iCuriosItemHandler.findFirstCurio(ItemInit.ICE_MATERIA.get()).isPresent()) {
                iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Check in slot one
                        if (slot0 == ice_materia
                                && !ice_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 2, true);
                          //  System.out.println("SLOT ONE IS ICE");
                        }
                        //Check in slot two
                        if (slot1 == ice_materia
                                && !ice_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 2, true);
                           // System.out.println("SLOT TWO IS ICE");
                        }
                    }
                });
            }
        });
//        //Check for ice materia

//        //Check for earth materia
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            if (iCuriosItemHandler.findFirstCurio(ItemInit.EARTH_MATERIA.get()).isPresent()) {
                iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Check in slot one
                        if (slot0 == earth_materia
                                && !earth_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 3, true);
                          //  System.out.println("SLOT ONE IS EARTH");
                        }
                        //Check in slot two
                        if (slot1 == earth_materia
                                && !earth_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 3, true);
                          //  System.out.println("SLOT TWO IS EARTH");
                        }
                    }
                });
            }
        });
        //Check for ice materia

        //Check for lightning materia
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            if (iCuriosItemHandler.findFirstCurio(ItemInit.LIGHTNING_MATERIA.get()).isPresent()) {
                iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Check in slot one
                        if (slot0 == lightning_materia
                                && !lightning_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 4, true);
                          //  System.out.println("SLOT ONE IS LIGHTNING");
                        }
                        //Check in slot two
                        if (slot1 == lightning_materia
                                && !lightning_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 4, true);
                          //  System.out.println("SLOT TWO IS LIGHTNING");
                        }
                    }
                });
            }
        });
        //Check for lightning materia

//        //Check for restore materia
        CuriosApi.getCuriosInventory(event.player).ifPresent(iCuriosItemHandler -> {
            if (iCuriosItemHandler.findFirstCurio(ItemInit.RESTORE_MATERIA.get()).isPresent()) {
                iCuriosItemHandler.getStacksHandler("linkedmateriaslot").ifPresent(stacks ->
                {
                    //Iterate through the linked slots
                    for (int i = 0; i < stacks.getStacks().getSlots(); i++) {
                        ItemStack slot0 = stacks.getStacks().getStackInSlot(0);
                        ItemStack slot1 = stacks.getStacks().getStackInSlot(1);
                        //Check in slot one
                        if (slot0 == restore_materia
                                && !restore_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_one(event.player, 5, true);
                         //   System.out.println("SLOT ONE IS RESTORE");
                        }
                        //Check in slot two
                        if (slot1 == restore_materia
                                && !restore_materia.isEmpty()) {
                            materia_stats.set_weapon_spell_slot_two(event.player, 5, true);
                           // System.out.println("SLOT TWO IS RESTORE");
                        }
                    }
                });
            }
        });
        //Check for restore materia
    }

}
