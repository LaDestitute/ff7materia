package com.ladestitute.ff7materia.event;

import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.ladestitute.ff7materia.network.packets.ClientPacketSendParticles;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import com.ladestitute.ff7materia.registry.ItemInit;
import com.ladestitute.ff7materia.util.FF7MateriaKeyboardUtil;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Silverfish;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.TickEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import net.neoforged.neoforge.network.PacketDistributor;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

import java.util.List;
import java.util.Optional;

public class MagicMateriaHandler {
    //WIP materia handler

    @SubscribeEvent
    public void firemateriaeffects(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        if (FF7MateriaKeyboardUtil.KP_ONE_KEY.consumeClick() && materia_stats.gettarget_count() == 1 && materia_stats.getweapon_spell_slot_one() == 1 &&
                materia_stats.getMP() >= 4) {
            materia_stats.setmp(event.player, materia_stats.getMP() - 4, true);
            AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(10);
            List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
            for (LivingEntity living : entities) {
                if (living.level().isClientSide) {
                    return;
                }
                ClientPacketSendParticles particles = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles.queueParticle(
                            ParticleTypes.FLAME,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() + 1 - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles);
                ClientPacketSendParticles particles1 = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles1.queueParticle(
                            ParticleTypes.FLAME,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() + 1 - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles1);
                if (living.getData(AttachmentInit.TARGETED)) {
                    //2.5f with the All materia, 1.25f if All materia hits a blaze/magma cube/etc
                    //1.9f if fire materia targets a blaze/magmacube/etc
                    living.hurt(living.damageSources().magic(), 3.8f);
                    living.setSecondsOnFire(3);
                }
            }
        }
    }

    @SubscribeEvent
    public void icemateriaeffects(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        if (FF7MateriaKeyboardUtil.KP_ONE_KEY.consumeClick() && materia_stats.gettarget_count() == 1 && materia_stats.getweapon_spell_slot_one() == 2 &&
                materia_stats.getMP() >= 4) {
            materia_stats.setmp(event.player, materia_stats.getMP() - 4, true);
            AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(10);
            List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
            for (LivingEntity living : entities) {
                if (living.level().isClientSide) {
                    return;
                }
                ClientPacketSendParticles particles = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles.queueParticle(
                            ParticleTypes.SNOWFLAKE,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() + 1 - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles);
                ClientPacketSendParticles particles1 = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles1.queueParticle(
                            ParticleTypes.SNOWFLAKE,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() + 1 - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles1);
                if (living.getData(AttachmentInit.TARGETED)) {
                    //2.5f with the All materia, 1.25f if All materia hits a stray/etc
                    //1.9f if ice materia targets a stray/etc
                    living.hurt(living.damageSources().magic(), 3.8f);
                }
            }
        }
    }

    @SubscribeEvent
    public void earthmateriaeffects(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);

        if (FF7MateriaKeyboardUtil.KP_ONE_KEY.consumeClick() && materia_stats.gettarget_count() == 1 && materia_stats.getweapon_spell_slot_one() == 3 &&
                materia_stats.getMP() >= 6) {
            materia_stats.setmp(event.player, materia_stats.getMP() - 6, true);
            AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(10);
            List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
            for (LivingEntity living : entities) {
                if (living.level().isClientSide) {
                    return;
                }
                ClientPacketSendParticles particles = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles.queueParticle(
                            ParticleTypes.LANDING_HONEY,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles);
                if (living.getData(AttachmentInit.TARGETED)) {
                    //3.7f with the All materia, 1.85f if All materia hits a stray/etc
                    //2.8f if earth materia targets a golem/etc
                    //allays, bats, parrots, bees, blazes, ghasts, phantoms, vex and breezes are immune to earth materia
                    living.hurt(living.damageSources().magic(), 5.6f);
                }
            }
        }
    }

    @SubscribeEvent
    public void lightningmateriaeffects(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        if (FF7MateriaKeyboardUtil.KP_ONE_KEY.consumeClick() && materia_stats.gettarget_count() == 1 && materia_stats.getweapon_spell_slot_one() == 4 &&
                materia_stats.getMP() >= 4) {
            materia_stats.setmp(event.player, materia_stats.getMP() - 4, true);
            AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(10);
            List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
            for (LivingEntity living : entities) {
                if (living.level().isClientSide) {
                    return;
                }
                ClientPacketSendParticles particles = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles.queueParticle(
                            ParticleTypes.ELECTRIC_SPARK,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles);
                if (living.getData(AttachmentInit.TARGETED)) {
                    //2.5f with the All materia, 1.25f if All materia hits a blaze/magma cube/etc
                    //1.9f if fire materia targets a blaze/magmacube/etc
                    LightningBolt bolt = EntityType.LIGHTNING_BOLT.create(living.level());
                    bolt.setPos(living.getX(), living.getY(), living.getZ());
                    bolt.setVisualOnly(true);
                    living.level().addFreshEntity(bolt);
                    living.hurt(living.damageSources().magic(), 3.8f);
                }
            }
        }
    }

    @SubscribeEvent
    public void restoremateriaeffects(TickEvent.PlayerTickEvent event) {
        PlayerMateriaStats materia_stats = event.player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        if (FF7MateriaKeyboardUtil.KP_ONE_KEY.consumeClick() && materia_stats.gettarget_count() == 1 && materia_stats.getweapon_spell_slot_one() == 5 &&
                materia_stats.getMP() >= 5) {
            materia_stats.setmp(event.player, materia_stats.getMP() - 5, true);
            AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(10);
            List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
            for (LivingEntity living : entities) {
                if (living.level().isClientSide) {
                    return;
                }
                ClientPacketSendParticles particles = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles.queueParticle(
                            ParticleTypes.ELECTRIC_SPARK,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles);
                ClientPacketSendParticles particles1 = new ClientPacketSendParticles();
                for (int j = 0; j < 50; j++) {
                    particles1.queueParticle(
                            ParticleTypes.ELECTRIC_SPARK,
                            false,
                            living.position().x() - 0.5D + living.getRandom().nextFloat(),
                            living.position().y() - 0.5D + living.getRandom().nextFloat(),
                            living.position().z() - 0.5D + living.getRandom().nextFloat(),
                            0D,
                            0D,
                            0D
                    );
                }
                PacketDistributor.TRACKING_ENTITY.with(living).send(particles1);
                //Todo: halve healing amount and add chance for a 'crit-heal'
                if(event.player.hasData(AttachmentInit.TARGETING))
                {
                    if (living.getData(AttachmentInit.TARGETED)) {
                        living.addEffect(new MobEffectInstance(MobEffects.HEAL, 20, 2));
                    }
                }
                else event.player.addEffect(new MobEffectInstance(MobEffects.HEAL, 20, 2));
            }
        }
    }

}
