package com.ladestitute.ff7materia.client.hud;

import com.ladestitute.ff7materia.FF7MateriaMain;
import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.RegisterGuiOverlaysEvent;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;
import net.neoforged.neoforge.client.gui.overlay.IGuiOverlay;
import net.neoforged.neoforge.client.gui.overlay.VanillaGuiOverlay;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.TickEvent;

@Mod.EventBusSubscriber(value= Dist.CLIENT, modid= FF7MateriaMain.MODID, bus= Mod.EventBusSubscriber.Bus.MOD)
public class MPGui implements IGuiOverlay
{

    private int mp;
    private int max_mp;
    private int target_count;
    private boolean targeting;

    @SubscribeEvent
    public static void registerOverlay(RegisterGuiOverlaysEvent event)
    {
        event.registerAbove(VanillaGuiOverlay.CHAT_PANEL.id(), new ResourceLocation("inventoryspam","inventoryspam.overlay"), new MPGui());
    }

    private final Minecraft mc = Minecraft.getInstance();

    public MPGui()
    {
        NeoForge.EVENT_BUS.addListener(this::clientTick);
    }


    public void clientTick(TickEvent.ClientTickEvent event)
    {
        if (event.phase != TickEvent.Phase.END)
            return;
    }

    public int debug = 1;

    @Override
    public void render(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int width, int height)
    {
        Minecraft minecraft = gui.getMinecraft();
        Player player = minecraft.player;
        PlayerMateriaStats materia_stats = player.getData(AttachmentInit.PLAYER_MATERIA_STATS);
        if(player.hasData(AttachmentInit.PLAYER_MATERIA_STATS))
        {
            mp = materia_stats.getMP();
            max_mp = materia_stats.getMaxMP();
            target_count = materia_stats.gettarget_count();
            targeting = materia_stats.is_targeting();
        }
        Font font = mc.font;
        var poseStack = graphics.pose();
        poseStack.pushPose();
        poseStack.scale(1, 1, 1);
        int color = 0xFFFFFF;
            RenderSystem.enableBlend();
                //  var label = Component.literal("MP: " + materia_stats.getMP() + "/" + materia_stats.getMaxMP());
            graphics.drawString(font, Component.literal("MP: " + mp + "/" + max_mp), Math.round((int) 362.5), 3, color);
            if(debug == 1 && targeting) {
                graphics.drawString(font, Component.literal("Targeting: yes"), Math.round((int) 342.5), 18, color);
            }
        if(debug == 1 && !targeting) {
            graphics.drawString(font, Component.literal("Targeting: no"), Math.round((int) 342.5), 18, color);
        }
        if(debug == 1 && target_count == 1) {
            graphics.drawString(font, Component.literal("Has target: yes"), Math.round((int) 342.5), 36, color);
        }
        if(debug == 1 && target_count == 0) {
            graphics.drawString(font, Component.literal("Has target: no"), Math.round((int) 342.5), 36, color);
        }
            poseStack.popPose();
    }

}