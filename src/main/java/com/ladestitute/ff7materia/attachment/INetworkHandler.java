package com.ladestitute.ff7materia.attachment;

import net.minecraft.network.FriendlyByteBuf;

public interface INetworkHandler {

    void write(FriendlyByteBuf buffer);

    void read(FriendlyByteBuf buffer);

}
