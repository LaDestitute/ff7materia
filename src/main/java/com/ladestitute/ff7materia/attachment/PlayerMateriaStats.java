package com.ladestitute.ff7materia.attachment;

import com.ladestitute.ff7materia.network.packets.UpdateMateriaStatsPacket;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.neoforge.common.util.INBTSerializable;
import net.neoforged.neoforge.network.PacketDistributor;

public class PlayerMateriaStats implements INetworkHandler, INBTSerializable<CompoundTag> {

    private int mp = 54;
    private int max_mp = 54;
    private int weapon_spell_slot_one;
    private int weapon_spell_slot_two;
    private boolean added_cut;
    private boolean added_cut_proc;
    private boolean is_targeting;
    private int target_count;

    public PlayerMateriaStats() {

    }

    //MP methods
    public void subtractmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.mp -= amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void addmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.mp += amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void setmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.mp = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }
    //MP methods

    //Max-MP methods
    public void subtractmaxmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.max_mp -= amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void addmaxmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.max_mp += amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void setmaxmp(LivingEntity entity, int amount, boolean canUpdate) {
        this.max_mp = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }
    //Max-MP methods

    //Spell slot methods
    public void set_weapon_spell_slot_one(LivingEntity entity, int amount, boolean canUpdate) {
        this.weapon_spell_slot_one = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void set_weapon_spell_slot_two(LivingEntity entity, int amount, boolean canUpdate) {
        this.weapon_spell_slot_two = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }
    //Spell slot methods

    //Added cut methods
    public void added_cut(LivingEntity entity, boolean amount, boolean canUpdate) {
        this.added_cut = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void added_cut_proc(LivingEntity entity, boolean amount, boolean canUpdate) {
        this.added_cut_proc = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }
    //Added cut methods

    //Targeting code
    public void is_targeting(LivingEntity entity, boolean amount, boolean canUpdate) {
        this.is_targeting = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }

    public void has_target(LivingEntity entity, int amount, boolean canUpdate) {
        this.target_count = amount;
        if(!entity.level().isClientSide()) {
            sendToClients(entity);
        }
    }
    //Targeting code

    public void tick(Entity parent) {
        if (parent instanceof ServerPlayer && parent.tickCount % 5 == 0) {
            //this fixes the mp-gui being zero on login, forces an update within a 1/4th-second of logging on
            sendToClients(parent);
        }
    }

    public int getMP() {
        return this.mp;
    }

    public int getMaxMP() {
        return this.max_mp;
    }

    public int getweapon_spell_slot_one() {
        return this.weapon_spell_slot_one;
    }

    public int getweapon_spell_slot_two() {
        return this.weapon_spell_slot_two;
    }

    public boolean getadded_cut() {
        return this.added_cut;
    }

    public boolean getadded_cut_proc() {
        return this.added_cut_proc;
    }

    public boolean is_targeting() { return this.is_targeting; }

    public int gettarget_count() {
        return this.target_count;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        nbt.putInt("mp", this.mp);
        nbt.putInt("max_mp", this.max_mp);
        nbt.putInt("weapon_spell_slot_one", this.weapon_spell_slot_one);
        nbt.putInt("weapon_spell_slot_two", this.weapon_spell_slot_two);
        nbt.putBoolean("added_cut", this.added_cut);
        nbt.putBoolean("added_cut_proc", this.added_cut_proc);
        nbt.putBoolean("is_targeting", this.is_targeting);
        nbt.putInt("target_count", this.target_count);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.mp = nbt.getInt("mp");
        this.max_mp = nbt.getInt("max_mp");
        this.weapon_spell_slot_one = nbt.getInt("weapon_spell_slot_one");
        this.weapon_spell_slot_two = nbt.getInt("weapon_spell_slot_two");
        this.added_cut = nbt.getBoolean("added_cut");
        this.added_cut_proc = nbt.getBoolean("added_cut_proc");
        this.is_targeting = nbt.getBoolean("is_targeting");
        this.target_count = nbt.getInt("target_count");
    }

    @Override
    public void write(FriendlyByteBuf buffer) {
        buffer.writeInt(mp);
        buffer.writeInt(max_mp);
        buffer.writeInt(weapon_spell_slot_one);
        buffer.writeInt(weapon_spell_slot_two);
        buffer.writeBoolean(added_cut);
        buffer.writeBoolean(added_cut_proc);
        buffer.writeBoolean(is_targeting);
        buffer.writeInt(target_count);
    }

    @Override
    public void read(FriendlyByteBuf buffer) {
        mp = buffer.readInt();
        max_mp = buffer.readInt();
        weapon_spell_slot_one = buffer.readInt();
        weapon_spell_slot_two = buffer.readInt();
        added_cut = buffer.readBoolean();
        added_cut_proc = buffer.readBoolean();
        is_targeting = buffer.readBoolean();
        target_count = buffer.readInt();
    }

    private void sendToClients(Entity parent) {
        PacketDistributor.TRACKING_ENTITY_AND_SELF.with(parent).send(new UpdateMateriaStatsPacket(this, parent));
    }

}