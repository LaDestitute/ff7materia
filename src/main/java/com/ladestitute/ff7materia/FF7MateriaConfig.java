package com.ladestitute.ff7materia;

import net.neoforged.neoforge.common.ModConfigSpec;

// An example config class. This is not required, but it's a good idea to have one to keep your config organized.
// Demonstrates how to use Forge's config APIs
public class FF7MateriaConfig
{
    public static final ModConfigSpec.Builder BUILDER = new ModConfigSpec.Builder();
    public static final ModConfigSpec SPEC;

    public static final ModConfigSpec.ConfigValue<Boolean> testboolean;
    public static final ModConfigSpec.IntValue testint;
    public static final ModConfigSpec.DoubleValue testdouble;

    static {
        BUILDER.push("Config");

        testboolean = BUILDER
                .comment("testboolean")
                .define("testboolean", true);
        testint = BUILDER
                .comment("testint")
                .defineInRange("testint", 16, 0, 100);
        testdouble = BUILDER
               .comment("testdouble")
               .defineInRange("testdouble", 1, 0, 10d);

        BUILDER.pop();
        SPEC = BUILDER.build();
    }

}
