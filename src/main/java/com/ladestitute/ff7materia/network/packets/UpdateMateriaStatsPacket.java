package com.ladestitute.ff7materia.network.packets;

import com.ladestitute.ff7materia.FF7MateriaMain;
import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;
import io.netty.buffer.Unpooled;

import javax.annotation.Nullable;

public record UpdateMateriaStatsPacket(@Nullable PlayerMateriaStats handler, int entity, @Nullable FriendlyByteBuf data) implements CustomPacketPayload {

    public static final ResourceLocation ID = new ResourceLocation(FF7MateriaMain.MODID, "ff7_materia_sync");

    public UpdateMateriaStatsPacket(PlayerMateriaStats handler, Entity entity) {
        this(handler, entity.getId(), null);
    }

    public UpdateMateriaStatsPacket(PlayerMateriaStats handler) {
        this(handler, -1, null);
    }

    public UpdateMateriaStatsPacket(FriendlyByteBuf packet) {
        this(null, packet.readInt(), packet);
    }

    @Override
    public void write(FriendlyByteBuf packet) {
        if (handler == null)
            throw new IllegalStateException("UpdateMateriaStatsPacket: Null Handler for entity id " + entity);
        packet.writeInt(entity);
        handler.write(packet);
    }

    @Override
    public ResourceLocation id() {
        return ID;
    }

    public static void handle(UpdateMateriaStatsPacket payload, PlayPayloadContext context) {
        context.player().ifPresent(player -> {
            Entity entity = payload.entity > 0 ? player.level().getEntity(payload.entity) : player;
            if (entity == null)
                return;
            FriendlyByteBuf data = payload.data;
            if (data == null && payload.handler != null) { // Assume Singleplayer
                data = new FriendlyByteBuf(Unpooled.buffer());
                payload.handler.write(data);
            }
            if (data != null)
                entity.getData(AttachmentInit.PLAYER_MATERIA_STATS).read(data);
        });
    }
}