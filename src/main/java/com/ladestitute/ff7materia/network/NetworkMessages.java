package com.ladestitute.ff7materia.network;

import com.ladestitute.ff7materia.FF7MateriaMain;
import com.ladestitute.ff7materia.network.packets.ClientPacketSendParticles;
import com.ladestitute.ff7materia.network.packets.UpdateMateriaStatsPacket;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlerEvent;
import net.neoforged.neoforge.network.registration.IPayloadRegistrar;

public class NetworkMessages {

    public static void register(IEventBus busMod) {
        busMod.addListener(RegisterPayloadHandlerEvent.class, event -> {
            IPayloadRegistrar network = event.registrar(FF7MateriaMain.MODID)
                    .versioned("1")
                    .optional();

            network.play(UpdateMateriaStatsPacket.ID, UpdateMateriaStatsPacket::new, side -> side.client(UpdateMateriaStatsPacket::handle));
            network.play(ClientPacketSendParticles.ID, ClientPacketSendParticles::new, side -> side.client(ClientPacketSendParticles::handle));
        });
    }

}