package com.ladestitute.ff7materia.registry;

import com.ladestitute.ff7materia.FF7MateriaMain;
import com.ladestitute.ff7materia.items.materia.*;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;

public class ItemInit {
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(FF7MateriaMain.MODID);

    //Magic materia
    public static final DeferredItem<Item> FIRE_MATERIA = ITEMS.register("fire_materia",
            () -> new FireMateriaItem(new Item.Properties()));
    public static final DeferredItem<Item> ICE_MATERIA = ITEMS.register("ice_materia",
            () -> new IceMateriaItem(new Item.Properties()));
    public static final DeferredItem<Item> EARTH_MATERIA = ITEMS.register("earth_materia",
            () -> new EarthMateriaItem(new Item.Properties()));
    public static final DeferredItem<Item> LIGHTNING_MATERIA = ITEMS.register("lightning_materia",
            () -> new LightningMateriaItem(new Item.Properties()));
    public static final DeferredItem<Item> RESTORE_MATERIA = ITEMS.register("restore_materia",
            () -> new RestoreMateriaItem(new Item.Properties()));

    //Support materia
    public static final DeferredItem<Item> ADDED_CUT_MATERIA = ITEMS.register("added_cut_materia",
            () -> new AddedCutMateriaItem(new Item.Properties()));

}
