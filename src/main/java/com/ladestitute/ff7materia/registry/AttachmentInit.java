package com.ladestitute.ff7materia.registry;

import com.ladestitute.ff7materia.FF7MateriaMain;
import com.ladestitute.ff7materia.attachment.PlayerMateriaStats;
import com.mojang.serialization.Codec;
import net.neoforged.neoforge.attachment.AttachmentType;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

import java.util.function.Supplier;

public class AttachmentInit {
    public static final DeferredRegister<AttachmentType<?>> ATTACHMENT_TYPES = DeferredRegister.create(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, FF7MateriaMain.MODID);

    //Most of these are likely to be squished into the materia-stats attachment once syncing works again
    public static final Supplier<AttachmentType<Boolean>> TARGETING = ATTACHMENT_TYPES.register(
            "targeting", () -> AttachmentType.builder(() -> false).serialize(Codec.BOOL).build());
    public static final Supplier<AttachmentType<Boolean>> TARGETED = ATTACHMENT_TYPES.register(
            "targeted", () -> AttachmentType.builder(() -> false).serialize(Codec.BOOL).build());
    public static final Supplier<AttachmentType<Boolean>> RECEIVING_MATERIA_DAMAGE = ATTACHMENT_TYPES.register(
            "receiving_materia_damage", () -> AttachmentType.builder(() -> false).serialize(Codec.BOOL).build());
    public static final Supplier<AttachmentType<Integer>> TARGET_LIMIT = ATTACHMENT_TYPES.register(
            "target_limit", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());
    public static final Supplier<AttachmentType<Integer>> MAGIC_POINTS = ATTACHMENT_TYPES.register(
            "magic_points", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());
    public static final Supplier<AttachmentType<Integer>> MAX_MAGIC_POINTS = ATTACHMENT_TYPES.register(
            "max_magic_points", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());
    public static final Supplier<AttachmentType<Boolean>> BASE_STATS_SET = ATTACHMENT_TYPES.register(
            "base_stats_set", () -> AttachmentType.builder(() -> false).serialize(Codec.BOOL).build());
    public static final Supplier<AttachmentType<Integer>> SPELL_SLOT_1 = ATTACHMENT_TYPES.register(
            "spell_slot_one", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());
    public static final Supplier<AttachmentType<Integer>> SPELL_SLOT_2 = ATTACHMENT_TYPES.register(
            "spell_slot_two", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());
    public static final Supplier<AttachmentType<Integer>> DAMAGE_TYPE_FLAG = ATTACHMENT_TYPES.register(
            "damage_type_flag", () -> AttachmentType.builder(() -> 0).serialize(Codec.INT).build());

    //Tinkering until fixed, trying to get all the relevant player stats all in one class
    //Edit: Syncing is fixed
    public static final Supplier<AttachmentType<PlayerMateriaStats>> PLAYER_MATERIA_STATS = ATTACHMENT_TYPES.register(
            "player_materia_stats", () -> AttachmentType.serializable(PlayerMateriaStats::new).copyOnDeath().build()
    );

}
