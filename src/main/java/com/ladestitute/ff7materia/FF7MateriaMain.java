package com.ladestitute.ff7materia;

import com.ladestitute.ff7materia.event.CoreMateriaHandler;
import com.ladestitute.ff7materia.event.MagicMateriaHandler;
import com.ladestitute.ff7materia.event.TargetingHandler;
import com.ladestitute.ff7materia.network.NetworkMessages;
import com.ladestitute.ff7materia.registry.AttachmentInit;
import com.ladestitute.ff7materia.registry.ItemInit;
import com.ladestitute.ff7materia.util.FF7MateriaKeyboardUtil;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.ItemLike;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.entity.living.LivingEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(FF7MateriaMain.MODID)
public class FF7MateriaMain
{
    // Define mod id in a common place for everything to reference
    public static final String MODID = "ff7materia";

    //Register the tabs
    public static final DeferredRegister<CreativeModeTab> TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, FF7MateriaMain.MODID);

    //Neoforge recommends adding items to tabs via .displayItems instead of the event as of now
    public static final List<Supplier<? extends ItemLike>> MATERIA_TAB_ITEMS = new ArrayList<>();
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> MATERIA_TAB = TABS.register("ff7materia_tab", () -> CreativeModeTab.builder()
            .title(Component.literal("FF7 Materia")) //The language key for the title of your CreativeModeTab
            .withTabsBefore(CreativeModeTabs.COMBAT)
            .icon(() -> ItemInit.FIRE_MATERIA.get().getDefaultInstance())
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.FIRE_MATERIA.get());
                output.accept(ItemInit.ICE_MATERIA.get());
                output.accept(ItemInit.EARTH_MATERIA.get());
                output.accept(ItemInit.LIGHTNING_MATERIA.get());
                output.accept(ItemInit.RESTORE_MATERIA.get());
            }).build());

    // FML will recognize some parameter types like IEventBus or ModContainer and pass them in automatically.
    public FF7MateriaMain(IEventBus modEventBus)
    {
        //Register items
        ItemInit.ITEMS.register(modEventBus);

        //Register attachments
        AttachmentInit.ATTACHMENT_TYPES.register(modEventBus);

        //Register the tabs
        TABS.register(modEventBus);

        //Needed to fix gui issues for the mp text (mp text not correcting on login)
        NeoForge.EVENT_BUS.addListener(LivingEvent.LivingTickEvent.class, event -> {
            event.getEntity().getData(AttachmentInit.PLAYER_MATERIA_STATS).tick(event.getEntity());
        });

        //Register events
        NeoForge.EVENT_BUS.register(new TargetingHandler());
        NeoForge.EVENT_BUS.register(new CoreMateriaHandler());
        NeoForge.EVENT_BUS.register(new MagicMateriaHandler());

        // Register our mod's ModConfigSpec so that FML can create and load the config file for us
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON,
                FF7MateriaConfig.SPEC, "ff7materia-config.toml");

        //Register keybinds
        if (FMLEnvironment.dist == Dist.CLIENT) {
            modEventBus.addListener(FF7MateriaKeyboardUtil::keyBind);
        }

        //Register network
        NetworkMessages.register(modEventBus);
    }

}
